import { Component, OnInit } from '@angular/core';
import { LoginuserService } from '../service/loginuser.service';
import { Route, Router } from '@angular/router';
import { Employee } from '../entities/employee';

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css'],
})
export class LoginUserComponent implements OnInit {
  employee: Employee = new Employee();
  data: any;
  status: any;
  myLabel!: string;
  display = false;
  displayCustomerButton = true;
  displayAgentButton = true;
  displayEmployeeButton = true;
  constructor(
    private loginUserService: LoginuserService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.myLabel = 'Login';
  }
  Customer() {
    this.myLabel = 'Customer Login';
    this.display = true;
    this.displayCustomerButton = false;
    this.displayAgentButton = false;
    this.displayEmployeeButton = false;
  }
  Employee() {
    this.myLabel = 'Employee Login';
    this.display = true;
    this.displayCustomerButton = false;
    this.displayAgentButton = false;
    this.displayEmployeeButton = false;
  }
  Agent() {
    this.myLabel = 'Agent Login';
    this.display = true;
    this.displayCustomerButton = false;
    this.displayAgentButton = false;
    this.displayEmployeeButton = false;
  }
  userLogin() {
    console.log(this.employee);
    this.loginUserService.loginUser(this.employee).subscribe(
      (data: any) => {
        this.data = data;
        this.status = data.status;
        if (this.status == 'Admin') {
          this.route.navigate(['/dashboard']);
          // alert('Login successfully');
        } else if (this.status == 'sales') {
          this.route.navigate(['/dashboardlead']);
          // alert('Login successfully');
        }
      }

      // (error) => alert('Sorry please enter correct username and password')
    );
  }
}

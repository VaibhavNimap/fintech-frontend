import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Lead } from '../entities/lead';
import { LeadService } from '../service/lead.service';

@Component({
  selector: 'app-update-lead',
  templateUrl: './update-lead.component.html',
  styleUrls: ['./update-lead.component.css'],
})
export class UpdateLeadComponent implements OnInit {
  lead: Lead = new Lead();
  id!: Number;
  constructor(
    private route: ActivatedRoute,
    private routes: Router,
    private leadService: LeadService
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.leadService.getLeadById(this.id).subscribe((data) => {
      this.lead = data;
    });
  }
  goToLeadList() {
    this.routes.navigate(['lead']);
  }
  onSubmit() {
    this.goToLeadList();
    this.leadService.updateLeadById(this.id, this.lead).subscribe((data) => {});
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Geography } from '../entities/geography';
import { GeographyService } from '../service/geography.service';

@Component({
  selector: 'app-update-geography',
  templateUrl: './update-geography.component.html',
  styleUrls: ['./update-geography.component.css'],
})
export class UpdateGeographyComponent implements OnInit {
  geography: Geography = new Geography();
  id!: Number;
  constructor(
    private route: ActivatedRoute,
    private routes: Router,
    private geographyService: GeographyService
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.geographyService.getGeographyById(this.id).subscribe((data) => {
      this.geography = data;
    });
  }
  goToGeographyList() {
    this.routes.navigate(['geography']);
  }
  onSubmit() {
    this.goToGeographyList();
    this.geographyService
      .updateGeographyById(this.id, this.geography)
      .subscribe((data) => {});
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateGeographyComponent } from './update-geography.component';

describe('UpdateGeographyComponent', () => {
  let component: UpdateGeographyComponent;
  let fixture: ComponentFixture<UpdateGeographyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateGeographyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGeographyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Role } from '../entities/role';
import { RoleService } from '../service/role.service';

@Component({
  selector: 'app-update-role',
  templateUrl: './update-role.component.html',
  styleUrls: ['./update-role.component.css'],
})
export class UpdateRoleComponent implements OnInit {
  role: Role = new Role();
  id!: Number;
  constructor(
    private roleService: RoleService,
    private route: ActivatedRoute,
    private routes: Router
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.roleService.getRoleById(this.id).subscribe((data) => {
      this.role = data;
    });
  }
  goToRoleList() {
    this.routes.navigate(['role']);
  }

  onSubmit() {
    this.goToRoleList();
    this.roleService.updateRoleById(this.id, this.role).subscribe((data) => {});
  }
}

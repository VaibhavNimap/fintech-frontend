import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../entities/employee';
import { EmployeeService } from '../service/employee.service';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css'],
})
export class EmployeesListComponent implements OnInit {
  decode!: Employee[];
  display = false;
  constructor(
    private employeeService: EmployeeService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.getEmployees();
  }
  private getEmployees() {
    this.employeeService.getEmployeeList().subscribe((data) => {
      this.decode = data;
    });
  }
  updateEmployee(id: Number) {
    this.route.navigate(['updateEmployee', id]);
  }
  deleteEmployee(id: Number) {
    this.employeeService.deleteEmployee(id).subscribe((data) => {});
    this.getEmployees();
  }
}

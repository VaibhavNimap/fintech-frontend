import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Branch } from '../entities/branch';
import { BranchService } from '../service/branch.service';

@Component({
  selector: 'app-create-branch',
  templateUrl: './create-branch.component.html',
  styleUrls: ['./create-branch.component.css'],
})
export class CreateBranchComponent implements OnInit {
  branch: Branch = new Branch();
  constructor(private route: Router, private branchService: BranchService) {}

  ngOnInit(): void {}
  saveBranch() {
    this.branchService.createBranch(this.branch).subscribe((data) => {
      console.log(data);
    });
  }
  goToBranchList() {
    this.route.navigate(['branch']);
  }
  onSubmit() {
    console.log(this.branch);
    this.saveBranch();
    this.goToBranchList();
  }
}

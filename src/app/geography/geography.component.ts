import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Geography } from '../entities/geography';
import { GeographyService } from '../service/geography.service';

@Component({
  selector: 'app-geography',
  templateUrl: './geography.component.html',
  styleUrls: ['./geography.component.css'],
})
export class GeographyComponent implements OnInit {
  geography!: any;

  constructor(
    private geographyService: GeographyService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.getGeography();
  }

  private getGeography() {
    this.geographyService.getGeographyList().subscribe((data) => {
      this.geography = data;
    });
  }

  updateGeography(id: Number) {
    this.route.navigate(['updateGeography', id]);
  }

  deleteGeography(id: Number) {
    this.geographyService.deleteGeography(id).subscribe((data) => {});
    this.getGeography();
  }
}

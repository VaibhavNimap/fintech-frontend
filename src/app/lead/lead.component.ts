import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Lead } from '../entities/lead';
import { LeadService } from '../service/lead.service';

@Component({
  selector: 'app-lead',
  templateUrl: './lead.component.html',
  styleUrls: ['./lead.component.css'],
})
export class LeadComponent implements OnInit {
  lead!: Lead[];
  first_Name: any;
  constructor(private leadService: LeadService, private route: Router) {}

  ngOnInit(): void {
    this.getLead();
  }
  private getLead() {
    this.leadService.getLeadList().subscribe((data) => {
      this.lead = data;
    });
  }
  updateLead(id: Number) {
    this.route.navigate(['updateLead', id]);
  }

  deleteLead(id: Number) {
    this.leadService.deleteLead(id).subscribe((data) => {});
    this.getLead();
  }
}

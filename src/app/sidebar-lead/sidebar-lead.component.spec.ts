import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarLeadComponent } from './sidebar-lead.component';

describe('SidebarLeadComponent', () => {
  let component: SidebarLeadComponent;
  let fixture: ComponentFixture<SidebarLeadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarLeadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarLeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

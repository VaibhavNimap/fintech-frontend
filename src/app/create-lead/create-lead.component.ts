import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Lead } from '../entities/lead';
import { LeadService } from '../service/lead.service';

@Component({
  selector: 'app-create-lead',
  templateUrl: './create-lead.component.html',
  styleUrls: ['./create-lead.component.css'],
})
export class CreateLeadComponent implements OnInit {
  lead: Lead = new Lead();
  constructor(private leadService: LeadService, private route: Router) {}

  ngOnInit(): void {}
  saveLead() {
    this.leadService.createLead(this.lead).subscribe((data) => {
      console.log(data);
    });
  }
  goToLeadList() {
    this.route.navigate(['lead']);
  }
  onSubmit() {
    console.log(this.lead);
    this.saveLead();
    this.goToLeadList();
  }
}

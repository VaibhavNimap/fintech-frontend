import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Branch } from '../entities/branch';
import { BranchService } from '../service/branch.service';

@Component({
  selector: 'app-update-branch',
  templateUrl: './update-branch.component.html',
  styleUrls: ['./update-branch.component.css'],
})
export class UpdateBranchComponent implements OnInit {
  branch: Branch = new Branch();
  id!: Number;
  constructor(
    private branchService: BranchService,
    private route: ActivatedRoute,
    private routes: Router
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.branchService.getBranchById(this.id).subscribe((data) => {
      this.branch = data;
    });
  }
  goToBranchList() {
    this.routes.navigate(['branch']);
  }
  onSubmit() {
    this.goToBranchList();
    this.branchService
      .updateBranchById(this.id, this.branch)
      .subscribe((data) => {});
  }
}

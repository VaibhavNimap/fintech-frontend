import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Branch } from '../entities/branch';
import { BranchService } from '../service/branch.service';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css'],
})
export class BranchComponent implements OnInit {
  branch!: Branch[];
  employeeService: any;
  constructor(private branchService: BranchService, private route: Router) {}

  ngOnInit(): void {
    this.getBranch();
  }

  private getBranch() {
    this.branchService.getBranchList().subscribe((data) => {
      this.branch = data;
    });
  }

  updateBranch(id: Number) {
    this.route.navigate(['updateBranch', id]);
  }

  deleteBranch(id: Number) {
    this.branchService.deleteBranch(id).subscribe((data) => {});
    this.getBranch();
  }
  getEmployees() {
    throw new Error('Method not implemented.');
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Geography } from '../entities/geography';
import { GeographyService } from '../service/geography.service';

@Component({
  selector: 'app-create-geography',
  templateUrl: './create-geography.component.html',
  styleUrls: ['./create-geography.component.css'],
})
export class CreateGeographyComponent implements OnInit {
  geography: Geography = new Geography();
  constructor(
    private geographyService: GeographyService,
    private route: Router
  ) {}

  ngOnInit(): void {}
  saveGeography() {
    this.geographyService.createGeography(this.geography).subscribe((data) => {
      console.log('saveGeography', data);
    });
  }
  goToGeographyList() {
    this.route.navigate(['geography']);
  }
  onSubmit() {
    console.log(this.geography);
    this.saveGeography();
    this.goToGeographyList();
  }
}

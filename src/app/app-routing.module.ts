import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BranchComponent } from './branch/branch.component';
import { CreateBranchComponent } from './create-branch/create-branch.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { CreateGeographyComponent } from './create-geography/create-geography.component';
import { CreateLeadComponent } from './create-lead/create-lead.component';
import { CreateRoleComponent } from './create-role/create-role.component';
import { DashboardLeadComponent } from './dashboard-lead/dashboard-lead.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EmployeesListComponent } from './employees-list/employees-list.component';
import { GeographyComponent } from './geography/geography.component';
import { LeadComponent } from './lead/lead.component';
import { LoginUserComponent } from './login-user/login-user.component';
import { RolesComponent } from './roles/roles.component';
import { SearchLeadComponent } from './search-lead/search-lead.component';
import { SidebarLeadComponent } from './sidebar-lead/sidebar-lead.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { UpdateBranchComponent } from './update-branch/update-branch.component';
import { UpdateEmployeeComponent } from './update-employee/update-employee.component';
import { UpdateGeographyComponent } from './update-geography/update-geography.component';
import { UpdateLeadComponent } from './update-lead/update-lead.component';
import { UpdateRoleComponent } from './update-role/update-role.component';
import { WorklistComponent } from './worklist/worklist.component';

const routes: Routes = [
  {
    path: 'employees',
    component: EmployeesListComponent,
  },
  {
    path: '',
    component: LoginUserComponent,
  },
  {
    path: 'createuser',
    component: CreateEmployeeComponent,
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
  },
  {
    path: 'sidebarlead',
    component: SidebarLeadComponent,
  },
  {
    path: 'lead',
    component: LeadComponent,
  },
  {
    path: 'dashboardlead',
    component: DashboardLeadComponent,
  },
  {
    path: 'craetelead',
    component: CreateLeadComponent,
  },
  {
    path: 'branch',
    component: BranchComponent,
  },
  {
    path: 'roles',
    component: RolesComponent,
  },
  {
    path: 'geography',
    component: GeographyComponent,
  },
  {
    path: 'createbranch',
    component: CreateBranchComponent,
  },
  {
    path: 'creategeography',
    component: CreateGeographyComponent,
  },
  {
    path: 'createrole',
    component: CreateRoleComponent,
  },
  {
    path: 'searchLead',
    component: SearchLeadComponent,
  },
  {
    path: 'updateEmployee/:id',
    component: UpdateEmployeeComponent,
  },
  {
    path: 'updateBranch/:id',
    component: UpdateBranchComponent,
  },
  {
    path: 'updateGeography/:id',
    component: UpdateGeographyComponent,
  },
  {
    path: 'updateRole/:id',
    component: UpdateRoleComponent,
  },
  {
    path: 'updateLead/:id',
    component: UpdateLeadComponent,
  },
  {
    path: 'worklist',
    component: WorklistComponent,
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
export const routingComponents = [
  EmployeesListComponent,
  LoginUserComponent,
  CreateEmployeeComponent,
  DashboardComponent,
  SidebarLeadComponent,
  LeadComponent,
  DashboardLeadComponent,
  CreateLeadComponent,
  BranchComponent,
  RolesComponent,
  GeographyComponent,
  CreateBranchComponent,
  CreateGeographyComponent,
  CreateRoleComponent,
  SearchLeadComponent,
  UpdateEmployeeComponent,
  UpdateBranchComponent,
  UpdateGeographyComponent,
  UpdateRoleComponent,
  UpdateLeadComponent,
  WorklistComponent,

];

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Role } from '../entities/role';
import { RoleService } from '../service/role.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css'],
})
export class RolesComponent implements OnInit {
  role!: Role[];
  constructor(private roleService: RoleService, private route: Router) {}

  ngOnInit(): void {
    this.getRole();
  }

  private getRole() {
    this.roleService.getRoleList().subscribe((data) => {
      this.role = data;
    });
  }

  updateRole(id: Number) {
    this.route.navigate(['updateRole', id]);
  }

  deleteRole(id: Number) {
    this.roleService.deleteRole(id).subscribe((data) => {});
    this.getRole();
  }
}

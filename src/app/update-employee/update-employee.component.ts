import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../entities/employee';
import { EmployeeService } from '../service/employee.service';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css'],
})
export class UpdateEmployeeComponent implements OnInit {
  constructor(
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private routes: Router
  ) {}
  employee: Employee = new Employee();
  id!: Number;
  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.employeeService.getEmployeeById(this.id).subscribe((data) => {
      this.employee = data;
    });
  }
  goToEmployeeList() {
    this.routes.navigate(['employees']);
  }
  onSubmit() {
    console.log(this.employee);
    this.goToEmployeeList();
    this.employeeService
      .updateEmployeeById(this.id, this.employee)
      .subscribe((data) => {});
  }
}

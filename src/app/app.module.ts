import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginUserComponent } from './login-user/login-user.component';
import { HttpClientModule } from '@angular/common/http';
import { EmployeesListComponent } from './employees-list/employees-list.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidebarLeadComponent } from './sidebar-lead/sidebar-lead.component';
import { LeadComponent } from './lead/lead.component';
import { DashboardLeadComponent } from './dashboard-lead/dashboard-lead.component';
import { CreateLeadComponent } from './create-lead/create-lead.component';
import { HeaderFooterComponent } from './header-footer/header-footer.component';
import { BranchComponent } from './branch/branch.component';
import { RolesComponent } from './roles/roles.component';
import { GeographyComponent } from './geography/geography.component';
import { CreateBranchComponent } from './create-branch/create-branch.component';
import { CreateRoleComponent } from './create-role/create-role.component';
import { CreateGeographyComponent } from './create-geography/create-geography.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SearchLeadComponent } from './search-lead/search-lead.component';
import { UpdateEmployeeComponent } from './update-employee/update-employee.component';
import { UpdateBranchComponent } from './update-branch/update-branch.component';
import { UpdateRoleComponent } from './update-role/update-role.component';
import { UpdateGeographyComponent } from './update-geography/update-geography.component';
import { UpdateLeadComponent } from './update-lead/update-lead.component';
import { WorklistComponent } from './worklist/worklist.component';



@NgModule({
  declarations: [AppComponent, routingComponents, SidebarComponent, DashboardComponent, SidebarLeadComponent, LeadComponent, DashboardLeadComponent, CreateLeadComponent, HeaderFooterComponent, BranchComponent, RolesComponent, GeographyComponent, CreateBranchComponent, CreateRoleComponent, CreateGeographyComponent, SearchLeadComponent, UpdateEmployeeComponent, UpdateBranchComponent, UpdateRoleComponent, UpdateGeographyComponent, UpdateLeadComponent, WorklistComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule, BrowserAnimationsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

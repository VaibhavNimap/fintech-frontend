import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Role } from '../entities/role';
import { LeadService } from '../service/lead.service';
import { RoleService } from '../service/role.service';

@Component({
  selector: 'app-create-role',
  templateUrl: './create-role.component.html',
  styleUrls: ['./create-role.component.css'],
})
export class CreateRoleComponent implements OnInit {
  role: Role = new Role();
  constructor(private route: Router, private roleService: RoleService) {}

  ngOnInit(): void {}
  saveRole() {
    this.roleService.createRole(this.role).subscribe((data) => {});
  }
  goToRoleList() {
    this.route.navigate(['roles']);
  }
  onSubmit() {
    console.log(this.role);
    this.saveRole();
    this.goToRoleList();
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Lead } from '../entities/lead';

@Injectable({
  providedIn: 'root',
})
export class LeadService {
  private baseUrl = 'http://localhost:8080/api/v1/lead';
  constructor(private httpClient: HttpClient) {}
  getLeadList(): Observable<Lead[]> {
    return this.httpClient.get<Lead[]>(`${this.baseUrl}`);
  }
  createLead(lead: Lead): Observable<object> {
    return this.httpClient.post(`${this.baseUrl}`, lead);
  }

  getLeadById(id: Number): Observable<Lead> {
    return this.httpClient.get<Lead>(`${this.baseUrl}/${id}`);
  }

  updateLeadById(id: Number, lead: Lead): Observable<object> {
    return this.httpClient.put(`${this.baseUrl}/${id}`, lead);
  }

  deleteLead(id: Number): Observable<object> {
    return this.httpClient.delete(`${this.baseUrl}/${id}`);
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../entities/employee';

@Injectable({
  providedIn: 'root',
})
export class LoginuserService {
  private baseUrl = 'http://localhost:8080/api/v1/login';
  constructor(private httpClient: HttpClient) {}
  loginUser(employee: Employee): Observable<object> {
    console.log(employee);
    return this.httpClient.post(`${this.baseUrl}`, employee);
  }
}

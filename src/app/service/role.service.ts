import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Lead } from '../entities/lead';
import { Role } from '../entities/role';

@Injectable({
  providedIn: 'root',
})
export class RoleService {
  private baseUrl = 'http://localhost:8080/api/v1/role';
  constructor(private httpClient: HttpClient) {}

  getRoleList(): Observable<Role[]> {
    return this.httpClient.get<Role[]>(`${this.baseUrl}`);
  }
  createRole(role: Role): Observable<object> {
    return this.httpClient.post(`${this.baseUrl}`, role);
  }

  getRoleById(id: Number): Observable<Role> {
    return this.httpClient.get<Role>(`${this.baseUrl}/${id}`);
  }

  updateRoleById(id: Number, role: Role): Observable<object> {
    return this.httpClient.put(`${this.baseUrl}/${id}`, role);
  }

  deleteRole(id: Number): Observable<object> {
    return this.httpClient.delete(`${this.baseUrl}/${id}`);
  }
}

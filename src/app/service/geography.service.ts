import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Geography } from '../entities/geography';

@Injectable({
  providedIn: 'root',
})
export class GeographyService {
  private baseUrl = 'http://localhost:8080/api/v1/geography';
  constructor(private httpClient: HttpClient) {}
  getGeographyList(): Observable<Geography[]> {
    return this.httpClient.get<Geography[]>(`${this.baseUrl}`);
  }

  createGeography(geography: Geography): Observable<object> {
    return this.httpClient.post(`${this.baseUrl}`, geography);
  }

  getGeographyById(id: Number): Observable<Geography> {
    return this.httpClient.get<Geography>(`${this.baseUrl}/${id}`);
  }

  updateGeographyById(id: Number, geography: Geography): Observable<object> {
    return this.httpClient.put(`${this.baseUrl}/${id}`, geography);
  }

  deleteGeography(id: Number): Observable<object> {
    return this.httpClient.delete(`${this.baseUrl}/${id}`);
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Branch } from '../entities/branch';

@Injectable({
  providedIn: 'root',
})
export class BranchService {
  private baseUrl = 'http://localhost:8080/api/v1/branch';
  constructor(private httpClient: HttpClient) {}
  getBranchList(): Observable<Branch[]> {
    return this.httpClient.get<Branch[]>(`${this.baseUrl}`);
  }
  createBranch(branch: Branch): Observable<object> {
    return this.httpClient.post(`${this.baseUrl}`, branch);
  }

  getBranchById(id: Number): Observable<Branch> {
    return this.httpClient.get<Branch>(`${this.baseUrl}/${id}`);
  }

  updateBranchById(id: Number, branch: Branch): Observable<object> {
    return this.httpClient.put(`${this.baseUrl}/${id}`, branch);
  }

  deleteBranch(id: Number): Observable<object> {
    return this.httpClient.delete(`${this.baseUrl}/${id}`);
  }
}

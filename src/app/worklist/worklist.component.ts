import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Lead } from '../entities/lead';
import { LeadService } from '../service/lead.service';

@Component({
  selector: 'app-worklist',
  templateUrl: './worklist.component.html',
  styleUrls: ['./worklist.component.css']
})
export class WorklistComponent implements OnInit {
  lead!: Lead[];
  display!: true;
  constructor(private leadService: LeadService, private route: Router) { }

  ngOnInit(): void {
    this.getLead();
  }
  private getLead() {
    this.leadService.getLeadList().subscribe((data) => {
      this.lead = data;
    });
}
}
import { Component, OnInit } from '@angular/core';
import { Lead } from '../entities/lead';
import { LeadService } from '../service/lead.service';

@Component({
  selector: 'app-search-lead',
  templateUrl: './search-lead.component.html',
  styleUrls: ['./search-lead.component.css'],
})
export class SearchLeadComponent implements OnInit {
  lead!: Lead[];
  first_Name: any;
  constructor(private leadService: LeadService) {}

  ngOnInit(): void {
    this.leadService.getLeadList().subscribe((data) => {
      this.lead = data;
    });
  }
  Search() {
    if (this.first_Name == '') {
      this.ngOnInit();
    } else {
      this.lead = this.lead.filter((data) => {
        return data.first_Name
          .toLocaleLowerCase()
          .match(this.first_Name.toLocaleLowerCase());
      });
    }
  }
}

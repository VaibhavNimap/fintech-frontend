export class Lead {
  id!: number;
  country!: string;
  state!: string;
  lead_Source!: string;
  email!: string;
  first_Name!: string;
  last_Name!: string;
  mobile_No!: string;
  pan!: string;
  aadhar!: string;
  date_Of_Birth!: string;
  postal_code!: string;
  city!: string;
  lead_Status!: string;
  assign_To!: string;
}

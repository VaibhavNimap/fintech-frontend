export class Employee {
  id!: number;
  name!: string;
  mobile_no!: string;
  email!: string;
  level!: string;
  role!: string;
  userName!: string;
  password!: string;
  status!: string;
}

export class Geography {
  id!: number;
  geographyName!: string;
  geographyCode!: string;
  postalCode!: string;
  city!: string;
  state!: string;
  country!: string;
}

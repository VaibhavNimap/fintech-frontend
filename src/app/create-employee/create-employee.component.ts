import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Subscriber } from 'rxjs';
import { Employee } from '../entities/employee';
import { EmployeeService } from '../service/employee.service';
import { LoginuserService } from '../service/loginuser.service';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css'],
})
export class CreateEmployeeComponent implements OnInit {
  employee: Employee = new Employee();
  constructor(
    private employeeServive: EmployeeService,
    private route: Router
  ) {}

  ngOnInit(): void {}
  saveEmployee() {
    this.employeeServive.createEmployee(this.employee).subscribe((data) => {
      console.log(data);
    });
  }
  goToEmployeeList() {
    this.route.navigate(['employees']);
  }
  onSubmit() {
    console.log(this.employee);
    this.saveEmployee();
    this.goToEmployeeList();
  }
}
